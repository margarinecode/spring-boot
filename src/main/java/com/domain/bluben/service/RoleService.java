package com.domain.bluben.service;

import com.domain.bluben.dto.RoleDto;
import com.domain.bluben.model.Role;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface RoleService {

    Role create(Role role);
    List<Role> findAll();
    Object findById(UUID id);

    RoleDto mapToDto(Role role);
    Role mapToEntity(RoleDto roleDto);

}
