package com.domain.bluben.service;

import com.domain.bluben.dto.RoleDto;
import com.domain.bluben.model.Role;
import com.domain.bluben.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public Role create(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    @Override
    public Object findById(UUID id) {
        Optional<Role> result = roleRepository.findById(id);
        return result.orElse(null);
    }

    @Override
    public RoleDto mapToDto(Role role) {
        return null;
    }

    @Override
    public Role mapToEntity(RoleDto roleDto) {
        return null;
    }
}
