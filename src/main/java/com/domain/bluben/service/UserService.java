package com.domain.bluben.service;

import com.domain.bluben.dto.UserDto;
import com.domain.bluben.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface UserService {

    User create(User user);
    User update(UUID id, User user);
    void delete(UUID id);

    Object login(String email, String password);

//    User loadUserByUsername(String username) throws UsernameNotFoundException;

    List<User> findAll();
    Object findById(UUID id);

    UserDto mapToDto(User user);
    User mapToEntity(UserDto userDto);
}
