package com.domain.bluben.service;

import com.domain.bluben.config.JwtTokenUtil;
import com.domain.bluben.dto.UserDto;
import com.domain.bluben.model.JwtResponse;
import com.domain.bluben.model.User;
import com.domain.bluben.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository;

    @Override
    public User create(User user) {
        return userRepository.save(user);
    }

    @Override
    public User update(UUID id, User user) {
        return userRepository.save(user);
    }

    @Override
    public void delete(UUID id) {

    }

    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @Override
    public Object login(String email, String password) {
//        String emailData = email.replace("%40", "@");
       User data =  userRepository.findByEmail(email);
       BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
       //&& encoder.matches(password, data.getPassword())
       if(data != null ){
           final String token = jwtTokenUtil.generateToken( data);
           final JwtResponse jwt =  new JwtResponse(token);
            return jwt;
//           return "ada";
       }else{
           throw new UsernameNotFoundException("User Not Found");
       }
    }


    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public Object findById(UUID id) {
        Optional<User> result =  userRepository.findById(id);
        return result.orElse(null);
    }

    @Override
    public UserDto mapToDto(User user) {
        return null;
    }

    @Override
    public User mapToEntity(UserDto userDto) {
        return null;
    }
}
