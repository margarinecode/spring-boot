package com.domain.bluben.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Table(name = "size")
//@SQLDelete(sql = "UPDATE category SET deleted_at=true WHERE id=?")
@Where(clause = "size0_.deleted_at = false")
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Size {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    public UUID id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(updatable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Column()
    public Boolean deleted_at;
}
