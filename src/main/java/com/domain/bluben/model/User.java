package com.domain.bluben.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.Modifying;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.UUID;

@Getter
@Setter
@Table(name = "users")
//@SQLDelete(sql = "UPDATE category SET deleted_at=true WHERE id=?")
//@Where(clause = "users0_.deleted_at = false")
@Entity
@EntityListeners(AuditingEntityListener.class)
public class User {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    public UUID id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name="email", nullable = false, unique = true)
    private String email;

    @Column()
    private String password;

    @Column()
    private UUID salt;

    @Column()
    private byte[] photo;

    @Column()
    private String imageType;

    @Column(updatable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Column()
    public Boolean deleted_at;

    @ManyToOne(targetEntity = Role.class,fetch = FetchType.EAGER)
    private Role role;

//    public String getUsername() {
//        return this.email;
//    }
//
//    public void setUsername(String username) {
//        this.email = username;
//    }
//
//    public String getPassword() {
//        return this.password;
//    }


//    public <E> User(String javainuse, String $2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6, ArrayList<E> es) {
//    }
}
