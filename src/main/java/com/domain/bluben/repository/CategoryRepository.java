package com.domain.bluben.repository;

import com.domain.bluben.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CategoryRepository extends JpaRepository<Category, UUID> {

    //Look up deleted entities
    @Query(value = "Select * from Category as e where lower(e.name) like lower(concat('%', :name,'%')) AND e.deleted_at=false LIMIT :pageSize  OFFSET :page ", nativeQuery = true)
    List<Category> searchName(@Param("name")String name,@Param("pageSize")Integer pageSize,@Param("page")Integer page);

    //
//    Soft delete.
    @Modifying
    @Query(value = "update Category as e  set deleted_at=true where id= ?1", nativeQuery = true)
    void softDelete(UUID id);


}
