package com.domain.bluben.repository;

import com.domain.bluben.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

        @Query(value = "Select * from Users  as e where e.email =:email", nativeQuery = true)
            User findByEmail(@Param("email")String email);
}
