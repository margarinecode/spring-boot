package com.domain.bluben.dto;

public record RoleDto(String name) {
}
