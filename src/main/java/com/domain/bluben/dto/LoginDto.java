package com.domain.bluben.dto;

public record LoginDto(String email, String password) {
}
