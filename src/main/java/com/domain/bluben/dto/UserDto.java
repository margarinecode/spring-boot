package com.domain.bluben.dto;

import java.util.UUID;

public record UserDto(String name, String email, String password, UUID role) {
}
