package com.domain.bluben.dto;

import org.springframework.web.multipart.MultipartFile;

public record ImageDto(MultipartFile image) {
}
