package com.domain.bluben.dto;

import java.util.UUID;

public record CategoryDto(String name) {
}
