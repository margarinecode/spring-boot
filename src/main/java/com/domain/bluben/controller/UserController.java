package com.domain.bluben.controller;

import com.domain.bluben.dto.ImageDto;
import com.domain.bluben.dto.LoginDto;
import com.domain.bluben.dto.UserDto;
import com.domain.bluben.helper.ResponseHandler;
import com.domain.bluben.model.Role;
import com.domain.bluben.model.User;
import com.domain.bluben.repository.UserRepository;
import com.domain.bluben.service.RoleService;
import com.domain.bluben.service.UserService;
import com.domain.bluben.util.ImageUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.multipart.MultipartFile;


import java.security.SecureRandom;
import java.util.UUID;

@Controller
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    UserRepository userRepository;

    @PostMapping("/create")
    public ResponseEntity<Object> signUp(@RequestBody UserDto request) {
        try {
            Role role = (Role) roleService.findById(request.role());
            User data = new User();
            data.setSalt(UUID.randomUUID());
            data.setDeleted_at(false);
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10, new SecureRandom());
            String hashedPassword = bCryptPasswordEncoder.encode(request.password());
            data.setPassword(hashedPassword);
            data.setName(request.name());
            data.setEmail(request.email());
//            byte[] img = ImageUtility.compressImage(file.getBytes());
//            data.setPhoto(img);
            data.setRole(role);
            User result = userService.create(data);
            return ResponseHandler.generateResponse("Successfully add data!", HttpStatus.OK, result);
        } catch (Exception e) {
            if (e.getMessage().equals("could not execute statement; SQL [n/a]; constraint [uk_3g1j96g94xpk3lpxl2qbl985x]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement")){
                return ResponseHandler.generateResponse("Name not available", HttpStatus.MULTI_STATUS, null);
            }else if(e.getMessage().equals("could not execute statement; SQL [n/a]; constraint [uk_6dotkott2kjsp8vw4d0m25fb7]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement")){
                return ResponseHandler.generateResponse("Email not available", HttpStatus.MULTI_STATUS, null);
            }

                return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }

    }

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody LoginDto request) {
        try {
            Object result = userService.login(request.email(), request.password());
            return ResponseHandler.generateResponse("success", HttpStatus.OK, result);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }


    @PutMapping("/update/{id}")
    public  ResponseEntity<Object> update(@PathVariable UUID id, @RequestParam() MultipartFile file){
        try{
            User a = (User) userService.findById(id);
//            file.getContentType()
            System.out.println(file.getContentType());
            if(a != null ){

                    a.setPhoto(ImageUtility.compressImage(file.getBytes()));
                    a.setImageType(file.getContentType());
                    userService.update(id, a);
                    return ResponseHandler.generateResponse("Succesfully change profile", HttpStatus.OK, a);

            }else{
                return ResponseHandler.generateResponse("Data Not Found", HttpStatus.NOT_FOUND, null);
            }
        }catch(Exception e){
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    @GetMapping(path = {"/get/image/{id}"})
    public ResponseEntity<byte[]> getImage(@PathVariable("id") UUID id){

    User a = (User) userService.findById(id);
    MediaType contentType = MediaType.IMAGE_JPEG;
    switch (a.getImageType()){
        case ("image/jpeg") : contentType = MediaType.IMAGE_JPEG;
        break;
        case ("image/jpg") : contentType = MediaType.IMAGE_JPEG;
        break;
        case("image/png") : contentType = MediaType.IMAGE_PNG;
        break;
    }

        return ResponseEntity
                .ok()
                .contentType(contentType)
                .body(ImageUtility.decompressImage(a.getPhoto()));
}
}
