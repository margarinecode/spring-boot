package com.domain.bluben.controller;

import com.domain.bluben.dto.RoleDto;
import com.domain.bluben.helper.ResponseHandler;
import com.domain.bluben.model.Role;
import com.domain.bluben.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Controller
@RestController
@RequestMapping("/role")
public class RoleController {
    @Autowired
    RoleService roleService;

    @PostMapping("/create")
    public ResponseEntity<Object> create(@RequestBody RoleDto request){
        try{
            Role role = new Role();
            role.setName(request.name());
            role.setDeleted_at(false);
            role.setId(UUID.randomUUID());
            Role result = roleService.create(role);
            return ResponseHandler.generateResponse("Successfully Created Data!", HttpStatus.OK, result);
        }catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    @GetMapping(value = "/detail/{id}")
    public ResponseEntity<Object> GetDetail(@PathVariable UUID id){
        try{
            Role a = (Role) roleService.findById(id);
            if(a != null){
                return ResponseHandler.generateResponse("Successfully Get Data!", HttpStatus.OK, a);
            }else {
                return ResponseHandler.generateResponse("Data Not Found", HttpStatus.NOT_FOUND, a);
            }
        }catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    @GetMapping("/")
    public ResponseEntity<Object> findAll(){
        try {
            List<Role> result = roleService.findAll();
            return ResponseHandler.generateResponse("Successfully Get Data!", HttpStatus.OK, result);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }
}
