package com.domain.bluben.controller;

import com.domain.bluben.dto.CategoryDto;
import com.domain.bluben.helper.ResponseHandler;
import com.domain.bluben.model.Category;
import com.domain.bluben.repository.CategoryRepository;
import com.domain.bluben.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
//import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.SecureRandom;
import java.util.List;
import java.util.UUID;


@Controller
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @Autowired
    CategoryRepository categoryRepository;


//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }


    @PostMapping("/create")
    public ResponseEntity<Object> create(@RequestBody CategoryDto request) {
        try {
            Category cat = new Category();
            cat.setName(request.name());
            cat.setDeleted_at(false);
            cat.setId(UUID.randomUUID());
            Category result = categoryService.create(cat);
//            CategoryDto data =  categoryService.mapToDto(result);
            return ResponseHandler.generateResponse("Successfully Created Data!", HttpStatus.CREATED, result);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(String.valueOf(e.getMessage()), HttpStatus.MULTI_STATUS, null);
        }


    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> update(@PathVariable UUID id, @RequestBody CategoryDto request) {
        try {
            Category a = (Category) categoryService.findById(id);
            if (a != null) {
                a.setName(request.name());
                categoryService.update(id, a);
                return ResponseHandler.generateResponse("Successfully Updated Data!", HttpStatus.OK, a);
            }else{
                return ResponseHandler.generateResponse("Data Not Found", HttpStatus.NOT_FOUND, null);
            }
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable UUID id) {
        try {
            Category a = (Category) categoryService.findById(id);
            if (a != null) {
                categoryService.delete(id);
                return ResponseHandler.generateResponse("Successfully deleted Data!", HttpStatus.OK, null);
            }else{
                return ResponseHandler.generateResponse("Data Not Found", HttpStatus.NOT_FOUND, null);
            }

        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }

    }

    @GetMapping("/list")
    public ResponseEntity<Object> findAll(@RequestParam(required = false) String name, @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer pageSize){
        try {
            if (page==null){
                page=1;
            }if (pageSize==null){
                pageSize=categoryRepository.findAll().size();
            }
            page-=1;
            var result= categoryService.findAll(name,pageSize,page);
            return ResponseHandler.generateResponse("Success", HttpStatus.OK, result);
        }catch (Exception e){
            return ResponseHandler.generateResponse("gagal", HttpStatus.BAD_REQUEST, null);
        }

    }

    @GetMapping(value = "/detail/{id}")
    public ResponseEntity<Object> Get(@PathVariable UUID id) {
        try {
            Category a = (Category) categoryService.findById(id);
            if (a != null) {
                return ResponseHandler.generateResponse("Successfully Get Data!", HttpStatus.OK, a);
            }else{
                return ResponseHandler.generateResponse("Data Not Found", HttpStatus.NOT_FOUND, null);
            }
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }

    }
}
